package ro.devis.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ro.devis.model.Administrator;
import ro.devis.model.Data;
import ro.devis.model.User;
import ro.devis.service.AdministratorService;
import ro.devis.service.DataService;
import ro.devis.utils.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RequestMapping("")
@Controller
@SessionAttributes("uid")
public class DataController {

    private DataService dataService;
    private AdministratorService administratorService;

    public DataController(DataService dataService, AdministratorService administratorService) {
        this.dataService = dataService;
        this.administratorService = administratorService;
    }

    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    public void setAdministratorService(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }

    @GetMapping("/data")
    @ResponseBody
    public String getIp(@RequestParam String ip) {
        return String.valueOf(dataService.getIP(ip));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/addIP", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String addIP(@RequestBody String json, HttpServletRequest request, HttpServletResponse response) throws Exception {
        JSONObject jsonObj = new JSONObject(json);
        Data data = new Data();
        data.setIp(jsonObj.getString("ip"));

        return String.valueOf(dataService.addIP(data));
    }

    @RequestMapping(value = "/dataList", method = RequestMethod.GET)
    public String showDataList(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

        if (!Security.checkUserAcces(request))
            return "redirect:/login";

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));
        model.addAttribute("username", currentUser.getUsername());

        model.addAttribute("listData", this.dataService.listIP());

        return "dataList";
    }


    @RequestMapping(value = "/addData", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));


        ModelAndView mav = new ModelAndView("addData");
        mav.addObject("data", new Data());

        mav.addObject("username", currentUser.getUsername());
        return mav;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/addData", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String addIP(@RequestBody String json, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

        JSONObject jsonObj = new JSONObject(json);
        Data data = new Data();
        data.setIp(jsonObj.getString("ip"));
        data.setMalicious(jsonObj.getInt("malicious"));

        HashMap<String, String> result = new HashMap<>();
        List<String> errors = new ArrayList<>();

        if (data.hasNull()) {
            errors.add("One field is incorrect");
        } else {
            boolean success = dataService.addIP(data);
            if (!success) {
                errors.add("Can't add user");
            }

        }
        if (!errors.isEmpty()) {
            result.put("success", "false");
            result.put("errors", (new JSONArray(errors)).toString());
        } else {
            result.put("success", "true");
        }

        JSONObject resultJson = new JSONObject(result);

        return resultJson.toString();
    }

    @RequestMapping(value = "/data/{id}", method = RequestMethod.GET)
    public String currentData(
            @PathVariable("id") int id,HttpServletRequest request, ModelMap model) {

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));
        model.addAttribute("username", currentUser.getUsername());


        model.addAttribute("ip",this.dataService.currentData(id).getIp());
        model.addAttribute("malicious",this.dataService.currentData(id).getMalicious());


        return "data";
    }

    @RequestMapping(value = "/data/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String deleteData(@PathVariable("id") Integer id) {

        return String.valueOf(dataService.deleteData(dataService.currentData(id)));
    }

    @RequestMapping(value = "/updateData/{id}", method = RequestMethod.GET)
    public ModelAndView getUpdateUser(
            @PathVariable("id") int id, HttpServletRequest request, HttpServletResponse response, ModelMap model) {

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));
        model.addAttribute("username", currentUser.getUsername());


        model.addAttribute("ip",this.dataService.currentData(id).getIp());
        model.addAttribute("malicious",this.dataService.currentData(id).getMalicious());

        ModelAndView mav = new ModelAndView("updateData");
        mav.addObject("data", new Data());

        return mav;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/updateData/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String updateUser(@PathVariable("id") Integer id, @RequestBody String json , HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
        JSONObject jsonObj = new JSONObject(json);
        Data data = new Data();
        data.setId_data(jsonObj.getInt("id_data"));
        data.setIp(jsonObj.getString("ip"));
        data.setMalicious(jsonObj.getInt("malicious"));

        HashMap<String, String> result = new HashMap<>();
        List<String> errors = new ArrayList<>();

        if (data.hasNull()) {
            errors.add("One field is incorrect");
        } else {
            boolean success = dataService.updateData(data);
            if (!success) {
                errors.add("Can't add user");
            }

        }
        if (!errors.isEmpty()) {
            result.put("success", "false");
            result.put("errors", (new JSONArray(errors)).toString());
        } else {
            result.put("success", "true");
        }

        JSONObject resultJson = new JSONObject(result);

        return resultJson.toString();
    }

}
