package ro.devis.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import ro.devis.model.Administrator;
import ro.devis.service.AdministratorService;
import ro.devis.utils.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequestMapping("")
@Controller
@SessionAttributes("uid")
public class HelloController {

    public HelloController(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }

    public void setUserService(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }

    private AdministratorService administratorService;

    @RequestMapping(value="/welcome", method = RequestMethod.GET)
    public String printHello(ModelMap model, HttpServletRequest request, HttpServletResponse response) throws Exception {

        if(!Security.checkUserAcces(request))
            return "redirect:/login";

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));
        model.addAttribute("username", currentUser.getUsername());

        return "hello";
    }

}

