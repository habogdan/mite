package ro.devis.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import ro.devis.model.Administrator;
import ro.devis.model.Login;
import ro.devis.service.AdministratorService;
import ro.devis.utils.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@RequestMapping("")
@Controller
@SessionAttributes("uid")
public class LoginController {

    public LoginController(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }

    public void setAdministratorServiceService(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }

    private AdministratorService administratorService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("administrator", new Administrator());
        mav.addObject("login", new Login());
        return mav;
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logout(WebRequest request, SessionStatus status) {
        status.setComplete();
        request.removeAttribute("uid", WebRequest.SCOPE_SESSION);
        return "redirect:/login";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/registerProcess", produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    public String  addAdministrator(@RequestBody String json, HttpServletRequest request, HttpServletResponse response) {
        JSONObject jsonObj = new JSONObject(json);
        Administrator administrator = new Administrator();
        administrator.setUsername(jsonObj.getString("username"));
        administrator.setPassword(jsonObj.getString("password"));
        administrator.setEmail(jsonObj.getString("email"));
        administrator.setPhone_number(jsonObj.getString("phone_number"));
        HashMap<String, String> result = new HashMap<>();

        List<String> errors = new ArrayList<>();

        if(administrator.hasNull() || !administrator.isValidEmailAddress()) {
            errors.add("Email invalid");
        }
        else{
            try {
                administrator.setPassword(Security.crypt(administrator.getPassword()));
                String x = Security.crypt(administrator.getPassword());
                int i = 0;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            boolean success =  administratorService.register(administrator);
            if(!success) {
                errors.add("User already exists");
            }
        }

        if(!errors.isEmpty()) {
            result.put("success", "false");
            result.put("errors", (new JSONArray(errors)).toString());
        } else {
            result.put("success", "true");
            request.getSession().setAttribute("uid", administrator.getId_administrator());
        }

        JSONObject resultJson = new JSONObject(result);

        return resultJson.toString();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/loginProcess", produces = { MediaType.APPLICATION_JSON_VALUE })
    @ResponseBody
    public String performLogin(@RequestBody String json, HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException {
        JSONObject jsonObj = new JSONObject(json);
        Login login = new Login();
        login.setUsername(jsonObj.getString("username"));
        login.setPassword(jsonObj.getString("password"));
        HashMap<String, String> result = new HashMap<>();
        try {
            login.setPassword(Security.crypt(login.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Administrator administrator = administratorService.validateAdministrator(login);
        if (null != administrator) {
            result.put("success", "true");
            request.getSession().setAttribute("uid", administrator.getId_administrator());
        } else {
            result.put("success", "false");
        }
        JSONObject resultJson = new JSONObject(result);

        return resultJson.toString();
    }

}
