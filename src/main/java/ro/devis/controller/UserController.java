package ro.devis.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ro.devis.model.Administrator;
import ro.devis.model.User;
import ro.devis.service.AdministratorService;
import ro.devis.service.UserService;
import ro.devis.utils.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RequestMapping("")
@Controller
@SessionAttributes("uid")
public class UserController {

    private UserService userService;
    private AdministratorService administratorService;

    public void setAdministratorService(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public UserController(UserService userService, AdministratorService administratorService) {
        this.userService = userService;
        this.administratorService = administratorService;
    }



    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));


        ModelAndView mav = new ModelAndView("addUser");
        mav.addObject("user", new User());

        mav.addObject("username", currentUser.getUsername());
        return mav;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/addUser", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String addUser(@RequestBody String json, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

        JSONObject jsonObj = new JSONObject(json);
        User user = new User();
        user.setFirst_name(jsonObj.getString("first_name"));
        user.setLast_name(jsonObj.getString("last_name"));
        user.setEmail(jsonObj.getString("email"));
        user.setMac_address(jsonObj.getString("mac_address"));
        user.setId_administrator((Integer) request.getSession().getAttribute("uid"));

        HashMap<String, String> result = new HashMap<>();
        List<String> errors = new ArrayList<>();

        if (user.hasNull()) {
            errors.add("One field is incorrect");
        } else {
             if (user.isValidEmailAddress()){
                 boolean success = userService.addUser(user);
                 if (!success) {
                     errors.add("Can't add user");
                 }
             } else {
                 errors.add("Email invalid");
             }
        }

        if (!errors.isEmpty()) {
            result.put("success", "false");
            result.put("errors", (new JSONArray(errors)).toString());
        } else {
            result.put("success", "true");
        }

        JSONObject resultJson = new JSONObject(result);

        return resultJson.toString();
    }

    @RequestMapping(value = "/userList", method = RequestMethod.GET)
    public String showDataList(HttpServletRequest request, HttpServletResponse response, ModelMap model){

        if(!Security.checkUserAcces(request))
            return "redirect:/login";

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));
        model.addAttribute("username", currentUser.getUsername());

        model.addAttribute("listUser", this.userService.listUser((Integer) request.getSession().getAttribute("uid")));

        return "userList";
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public String getBarBySimplePathWithExplicitRequestParam(
            @PathVariable("id") int id,HttpServletRequest request, ModelMap model) {

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));
        model.addAttribute("username", currentUser.getUsername());


        model.addAttribute("first_name",this.userService.currentUser(id).getFirst_name());
        model.addAttribute("last_name",this.userService.currentUser(id).getLast_name());
        model.addAttribute("email",this.userService.currentUser(id).getEmail());
        model.addAttribute("mac_address",this.userService.currentUser(id).getMac_address());

        return "user";
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String deleteTechnology(@PathVariable("id") Integer id) {
        
       return String.valueOf(userService.deleteUser(userService.currentUser(id)));
    }

    @RequestMapping(value = "/updateUser/{id}", method = RequestMethod.GET)
    public ModelAndView getUpdateUser(
            @PathVariable("id") int id, HttpServletRequest request, HttpServletResponse response, ModelMap model) {

        Administrator currentUser = administratorService.currentAdministrator(request.getSession().getAttribute("uid"));
        model.addAttribute("username", currentUser.getUsername());


        model.addAttribute("first_name",this.userService.currentUser(id).getFirst_name());
        model.addAttribute("last_name",this.userService.currentUser(id).getLast_name());
        model.addAttribute("email",this.userService.currentUser(id).getEmail());
        model.addAttribute("mac_address",this.userService.currentUser(id).getMac_address());

        ModelAndView mav = new ModelAndView("updateUser");
        mav.addObject("user", new User());

        return mav;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/updateUser/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public String updateUser(@PathVariable("id") Integer id, @RequestBody String json , HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
        JSONObject jsonObj = new JSONObject(json);
        User user = new User();
        user.setId_user(jsonObj.getInt("id_user"));
        user.setFirst_name(jsonObj.getString("first_name"));
        user.setLast_name(jsonObj.getString("last_name"));
        user.setEmail(jsonObj.getString("email"));
        user.setMac_address(jsonObj.getString("mac_address"));
        user.setId_administrator((Integer) request.getSession().getAttribute("uid"));

        HashMap<String, String> result = new HashMap<>();
        List<String> errors = new ArrayList<>();

        if (user.hasNull()) {
            errors.add("One field is incorrect");
        } else {
            if (user.isValidEmailAddress()){
                boolean success = userService.updateUser(user);
                if (!success) {
                    errors.add("Can't add user");
                }
            } else {
                errors.add("Email invalid");
            }
        }

        if (!errors.isEmpty()) {
            result.put("success", "false");
            result.put("errors", (new JSONArray(errors)).toString());
        } else {
            result.put("success", "true");
        }

        JSONObject resultJson = new JSONObject(result);

        return resultJson.toString();
    }
}
