package ro.devis.dao;

import ro.devis.model.Administrator;
import ro.devis.model.Login;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface AdministratorDAO {

    public List<Administrator> listAdministrators();

    boolean register(Administrator administrator);

    Administrator validateAdministrator(Login login) throws NoSuchAlgorithmException;

    Administrator currentAdministrator(Object id);
}
