package ro.devis.dao;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ro.devis.model.Administrator;
import ro.devis.model.Login;


import javax.sql.DataSource;
import java.util.List;

@Repository
public class AdministratorDAOImpl implements AdministratorDAO {

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Administrator> listAdministrators() {
        Session session = this.sessionFactory.getCurrentSession();
        return (List<Administrator>) session.createQuery("from Administrator").list();
    }

    @Autowired
    DataSource datasource;
    @Autowired
    JdbcTemplate jdbcTemplate;
    public boolean register ( Administrator administrator){
        Query queryAdministrator = sessionFactory.getCurrentSession()
                .createQuery("select count(*) from Administrator where username = :username or email = :email");
        queryAdministrator.setParameter("username", administrator.getUsername());
        queryAdministrator.setParameter("email", administrator.getEmail());
        Long count = (Long)queryAdministrator.uniqueResult();

        if(count.longValue() != 0)
            return false;

        sessionFactory.getCurrentSession().save(administrator);

        return true;
    }

    public Administrator validateAdministrator(Login login) {

        String sql = "from Administrator where username= :username and password= :pass";
        List result = sessionFactory.getCurrentSession()
                .createQuery(sql)
                .setParameter("username", login.getUsername())
                .setParameter("pass", login.getPassword()).list();

        return result.size() > 0 ? (Administrator) result.get(0) : null;
    }

    public Administrator currentAdministrator(Object id){

        Query queryAdministrator = sessionFactory.getCurrentSession()
                .createQuery("from Administrator where id_administrator = :id_administrator");
        queryAdministrator.setParameter("id_administrator", id);

        return (Administrator) queryAdministrator.uniqueResult();
    }
}
