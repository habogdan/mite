package ro.devis.dao;

import ro.devis.model.Data;

import java.util.List;

public interface DataDAO {

    boolean getIP(String ip);
    List<Data> listIP();
    boolean addIP(Data data);
    Data currentData(int id);
    boolean deleteData(Data data);
    boolean updateData(Data data);

}
