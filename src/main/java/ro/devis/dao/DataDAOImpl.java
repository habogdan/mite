package ro.devis.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import ro.devis.model.Data;
import ro.devis.model.User;

import java.util.List;


@Repository
public class DataDAOImpl implements DataDAO {

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private SessionFactory sessionFactory;

    @Override
    public boolean getIP(String ip) {
        String sql = "from Data where ip= :ip";
        List<Data> result = (List<Data>) sessionFactory.getCurrentSession()
                .createQuery(sql)
                .setParameter("ip", ip).list();

        return result.size() > 0;
    }

    @Override
    public List<Data> listIP() {
        String sql = "from Data";
        List<Data> result = (List<Data>) sessionFactory.getCurrentSession()
                .createQuery(sql)
                .list();

        return result.size() > 0 ? (List<Data>) result : null;
    }

    @Override
    public boolean addIP(Data data) {
        Session session = this.sessionFactory.getCurrentSession();
        sessionFactory.getCurrentSession().save(data);

        return true;
    }

    @Override
    public Data currentData(int id) {
        Query queryData = sessionFactory.getCurrentSession()
                .createQuery("from Data where id_data =:id");
        queryData.setParameter("id", id);
        Data data = (Data) queryData.uniqueResult();

        return data;
    }

    @Override
    public boolean deleteData(Data data) {

        sessionFactory.getCurrentSession().delete(data);

        return true;
    }

    @Override
    public boolean updateData(Data data) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("update Data set ip =:ip, " +
                "malicious =:malicious where id_data = :id_data ");
        query.setString("id_data", String.valueOf(data.getId_data()));
        query.setString("ip", data.getIp());
        query.setString("malicious", String.valueOf(data.getMalicious()));
        int result = query.executeUpdate();
        if (result != 0)
            return true;
        return false;
    }


}
