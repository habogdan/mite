package ro.devis.dao;

import ro.devis.model.User;

import java.util.List;

public interface UserDAO {

    boolean addUser(User user);
    List<User> listUser(int id_administrator);
    User currentUser(int id);
    boolean deleteUser(User user);
    boolean updateUser(User user);
}
