package ro.devis.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import ro.devis.model.User;

import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private SessionFactory sessionFactory;

    @Override
    public boolean addUser(User user) {

        sessionFactory.getCurrentSession().save(user);

        return true;
    }

    @Override
    public List<User> listUser(int id_administrator) {
        Session session = this.sessionFactory.getCurrentSession();
        String sql = "from User where id_administrator= :id_administrator";
        List<User> result = (List<User>) sessionFactory.getCurrentSession()
                .createQuery(sql)
                .setParameter("id_administrator", id_administrator).list();


        return result.size() > 0 ? (List<User>) result : null;
    }

    @Override
    public User currentUser(int id) {

        Query queryProduct = sessionFactory.getCurrentSession()
                .createQuery("from User where id_user =:id");
        queryProduct.setParameter("id", id);
        User user = (User) queryProduct.uniqueResult();

        return user;
    }

    @Override
    public boolean deleteUser(User user) {

        sessionFactory.getCurrentSession().delete(user);

        return true;
    }

    @Override
    public boolean updateUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("update User set first_name =:first_name, " +
                "last_name =:last_name, email =:email, mac_address=:mac_address  where id_user = :id_user ");
        query.setString("id_user", String.valueOf(user.getId_user()));
        query.setString("first_name", user.getFirst_name());
        query.setString("last_name", user.getLast_name());
        query.setString("email", user.getEmail());
        query.setString("mac_address", user.getMac_address());
        int result = query.executeUpdate();
        if (result != 0)
            return true;
        return false;
    }
}
