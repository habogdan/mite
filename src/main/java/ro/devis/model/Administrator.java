package ro.devis.model;


import org.springframework.stereotype.Component;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.*;

@Entity
@Table(name = "ADMINISTRATORS", schema = "MITE")
@Component
public class Administrator {

    @Id
    @Column(name = "ID_ADMINISTRATOR")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_administrator;

    @Column(name = "USERNAME")
    private String username;

    @Column(name="PASSWORD")
    private String password;

    @Column(name="EMAIL")
    private String email;

    @Column(name = "PHONE_NUMBER", columnDefinition = "text")
    private String phone_number;

    public int getId_administrator() {
        return id_administrator;
    }

    public void setId_administrator(int id_administrator) {
        this.id_administrator = id_administrator;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public boolean isValidEmailAddress() {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public boolean hasNull() {
        return username.equals("") || password.equals("") || email.equals("") || phone_number.equals("");
    }

    @Override
    public String toString() {
        return "id_administrator=" + id_administrator + ", name=" + username;
    }
}
