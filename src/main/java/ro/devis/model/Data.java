package ro.devis.model;


import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Table(name = "DATA", schema = "MITE")
@Component
public class Data {

    @Id
    @Column(name = "ID_DATA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_data;

    @Column(name = "ip")
    private String ip;

    @Column(name = "malicious")
    private int malicious;

    public int getId_data() {
        return id_data;
    }

    public void setId_data(int id_data) {
        this.id_data = id_data;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getMalicious() {
        return malicious;
    }

    public void setMalicious(int malicious) {
        this.malicious = malicious;
    }

    public boolean hasNull() {
        return ip.equals("");
    }

    @Override
    public String toString() {
        return "id_data=" + id_data + ", malicious=" + malicious;
    }
}
