package ro.devis.service;

import ro.devis.model.Administrator;
import ro.devis.model.Login;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface AdministratorService {

    public List<Administrator> listAdministrators();

    boolean register(Administrator administrator);

    Administrator validateAdministrator(Login login) throws NoSuchAlgorithmException;

    Administrator currentAdministrator(Object id);

}
