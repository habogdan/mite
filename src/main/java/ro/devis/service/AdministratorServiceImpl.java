package ro.devis.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.devis.dao.AdministratorDAO;
import ro.devis.model.Administrator;
import ro.devis.model.Login;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Component
@Service
@Transactional
public class AdministratorServiceImpl implements AdministratorService {

    public void setAdministratorDAO(AdministratorDAO administratorDAO) {
        this.administratorDAO = administratorDAO;
    }

    private AdministratorDAO administratorDAO;

    @Override
    @Transactional
    public List<Administrator> listAdministrators() {
        return this.administratorDAO.listAdministrators();
    }

    @Override
    public boolean register(Administrator administrator) {
        return administratorDAO.register(administrator);
    }

    @Override
    public Administrator validateAdministrator(Login login) throws NoSuchAlgorithmException {
        return administratorDAO.validateAdministrator(login);
    }

    @Override
    public Administrator currentAdministrator(Object id){
        return this.administratorDAO.currentAdministrator(id);
    }

}
