package ro.devis.service;

import ro.devis.model.Data;
import ro.devis.model.User;

import java.util.List;

public interface DataService {

    boolean getIP(String ip);
    List<Data> listIP();
    boolean addIP(Data data);
    Data currentData(int id);
    boolean deleteData(Data data);
    boolean updateData(Data data);
}
