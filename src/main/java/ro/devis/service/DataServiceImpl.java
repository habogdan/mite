package ro.devis.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.devis.dao.DataDAO;
import ro.devis.dao.UserDAO;
import ro.devis.model.Data;

import java.util.List;

@Component
@Service
@Transactional
public class DataServiceImpl implements DataService{


    public void setDataDAO(DataDAO dataDAO) {
        this.dataDAO = dataDAO;
    }

    private DataDAO dataDAO;

    @Override
    public boolean getIP(String ip){
        return dataDAO.getIP(ip);
    }

    @Override
    public List<Data> listIP() {
        return dataDAO.listIP();
    }

    @Override
    public boolean addIP(Data data) {
        return dataDAO.addIP(data);
    }

    @Override
    public Data currentData(int id) {
        return dataDAO.currentData(id);
    }

    @Override
    public boolean deleteData(Data data) {
        return dataDAO.deleteData(data);
    }

    @Override
    public boolean updateData(Data data) {
        return dataDAO.updateData(data);
    }
}
