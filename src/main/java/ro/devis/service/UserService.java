package ro.devis.service;

import ro.devis.model.Data;
import ro.devis.model.User;

import java.util.List;

public interface UserService {

    boolean addUser(User user);
    List<User> listUser(int id_administrator);
    User currentUser(int id);
    boolean deleteUser(User user);
    boolean updateUser(User user);

}
