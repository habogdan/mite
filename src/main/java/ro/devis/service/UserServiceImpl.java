package ro.devis.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.devis.dao.UserDAO;
import ro.devis.model.User;

import java.util.List;

@Component
@Service
@Transactional
public class UserServiceImpl implements UserService{


    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    private UserDAO userDAO;

    @Override
    public boolean addUser(User user) {
        return userDAO.addUser(user);
    }

    @Override
    public List<User> listUser(int id_administrator) {
        return userDAO.listUser(id_administrator);
    }

    @Override
    public User currentUser(int id) {
        return userDAO.currentUser(id);
    }

    @Override
    public boolean deleteUser(User user) {
        return userDAO.deleteUser(user);
    }

    @Override
    public boolean updateUser(User user) {
        return userDAO.updateUser(user);
    }
}
