<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Data</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/home.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/table.css" rel="stylesheet">
</head>
<body>
<nav class="navbar-zone">
    <div class="navbar-left">
        <a href="/welcome"><span class="navbar-text-left">
            <img src="${pageContext.request.contextPath}/resources/pictures/Logo/LogoNameRight.png" class="navbar-logo"
                 alt="MITE Logo"></span> </a>
        <a href="/welcome" class="inactive hov-left"><span class="navbar-text-left-word">Home</span> </a>
        <a href="/userList" class="inactive hov-left"><span class="navbar-text-left-word">User</span> </a>
        <a href="/dataList" class="active hov-left"><span class="navbar-text-left-word">Ip</span> </a>
    </div>
    <div class="navbar-right">
        <div class="navbar-right-user">
            <a href="#" class="hov-right">
                <i class="fa fa-user user-icon" aria-hidden="true"></i>
                <span class="navbar-text-right-word">${username}</span>
            </a>
        </div>
        <button class="logout-button" name="logout" onclick="goToLinkLogout()">Logout</button>
    </div>
</nav>
<hr class="line">

<div class="col my-content">
    <div class="col table-content">
        <div class="row table-content-header">
            <span class="table-title">IP</span>
            <button class="table-right-button" name="addUser" onclick="goToLinkAddIP()">Add IP</button>
        </div>
        <div class="table-zone">
            <table class="table">
                <thead>
                <tr class="table-header">
                    <th class="table-header-text" scope="col">#</th>
                    <th class="table-header-text" scope="col">IP</th>
                    <th class="table-header-text" scope="col">Malicious</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${listData}" var="data">
                    <tr class="table-row" onclick="window.location='data/${data.id_data}'">
                        <th class="table-text" scope="row">${listData.indexOf(data)}</th>
                        <td class="table-text">${data.ip}</td>
                        <c:if test = "${data.malicious == 0}">
                            <td class="table-text">Unknown</td>
                        </c:if>
                        <c:if test = "${data.malicious == -1}">
                            <td class="table-text">Malicious</td>
                        </c:if>
                        <c:if test = "${data.malicious == 1}">
                            <td class="table-text">Clean</td>
                        </c:if>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="bottom-img-zone">
    <img src="${pageContext.request.contextPath}/resources/pictures/7.2.png" class="bottom-img"
         alt="MiteBottomImg">
</div>

</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<script>
    function goToLinkLogout() {
        window.location.href = "/logout"
    }

    function goToLinkAddIP() {
        window.location.href = "/addData"
    }
</script>
</html>
