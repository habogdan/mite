<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/home.css" rel="stylesheet">
</head>
<body>
<nav class="navbar-zone">
    <div class="navbar-left">
        <a href="/welcome"><span class="navbar-text-left">
            <img src="${pageContext.request.contextPath}/resources/pictures/Logo/LogoNameRight.png" class="navbar-logo"
                 alt="MITE Logo"></span> </a>
        <a href="/welcome" class="active hov-left"><span class="navbar-text-left-word">Home</span> </a>
        <a href="/userList" class="inactive hov-left"><span class="navbar-text-left-word">User</span> </a>
        <a href="/dataList" class="inactive hov-left"><span class="navbar-text-left-word">Ip</span> </a>
    </div>
    <div class="navbar-right">
        <div class="navbar-right-user">
            <a href="#" class="hov-right">
                <i class="fa fa-user user-icon" aria-hidden="true"></i>
                <span class="navbar-text-right-word">${username}</span>
            </a>
        </div>
        <button class="logout-button" name="logout" onclick="goToLinkLogout()">Logout</button>
    </div>
</nav>
<hr class="line">

</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<script>
    function goToLinkLogout() {
        window.location.href = "/logout"
    }
</script>
</html>
