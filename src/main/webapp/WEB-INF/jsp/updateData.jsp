<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/home.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/addItems.css" rel="stylesheet">
</head>
<body>
<nav class="navbar-zone">
    <div class="navbar-left">
        <a href="/welcome"><span class="navbar-text-left">
            <img src="${pageContext.request.contextPath}/resources/pictures/Logo/LogoNameRight.png" class="navbar-logo"
                 alt="MITE Logo"></span> </a>
        <a href="/welcome" class="inactive hov-left"><span class="navbar-text-left-word">Home</span> </a>
        <a href="/userList" class="inactive hov-left"><span class="navbar-text-left-word">User</span> </a>
        <a href="/dataList" class="active hov-left"><span class="navbar-text-left-word">Ip</span> </a>
    </div>
    <div class="navbar-right">
        <div class="navbar-right-user">
            <a href="#" class="hov-right">
                <i class="fa fa-user user-icon" aria-hidden="true"></i>
                <span class="navbar-text-right-word">${username}</span>
            </a>
        </div>
        <button class="logout-button" name="logout" onclick="goToLinkLogout()">Logout</button>
    </div>
</nav>
<hr class="line">
<%--CONTENT HERE--%>
<div class="my-content-item">

    <div class="col add-item-content">
        <div class="row add-item-content-header">
            <span class="add-item-title">Update Data: ${ip}</span>
        </div>
        <div class="row add-item-complete-img">
            <div class="col add-item-complete-zone">
                <form:form id="updateData" modelAttribute="data" action="updateData" method="put">

                    <form:label cssClass="row input-text" path="ip">IP</form:label>
                    <form:input cssClass="row input-input" path="ip" name="ip" id="ip"
                                placeholder="IP" value="${ip}"/>

                    <form:label cssClass="row input-text" path="malicious">Malicious</form:label>
                    <%--                    <form:input cssClass="row input-input" path="malicious" name="malicious" id="malicious"--%>
                    <%--                                placeholder="Malicious"/>--%>
                    <form action="/action_page.php">
                        <select id="malicious" name="malicious" class="input-input-select">
                            <option value="Unknown">Unknown</option>
                            <option value="Malicious">Malicious</option>
                            <option value="Clean">Clean</option>
                        </select>
                    </form>
                </form:form>
                <div class="row justify-content-center add-item-button-zone">
                    <button id="updateData" class="add-item-button" type="submit"  name="updateData">Update Data</button>
                    <%--                    <form:button id="addUser" cssClass="add-item-button" name="addUser">Add User</form:button>--%>
                </div>
            </div>
            <div class="add-item-img-zone">
                <img src="${pageContext.request.contextPath}/resources/pictures/3.1.png" class="add-item-img"
                     alt="MiteCompleteImg">
            </div>
        </div>
    </div>
</div>
<%--END CONTENT HERE--%>
<div class="bottom-img-zone">
    <img src="${pageContext.request.contextPath}/resources/pictures/7.2.png" class="bottom-img"
         alt="MiteBottomImg">
</div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

<script src="${pageContext.request.contextPath}/resources/js/updateData.js"></script>
<script>
    function goToLinkLogout() {
        window.location.href = "/logout"
    }
</script>
</html>
