<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/home.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/addItems.css" rel="stylesheet">
</head>
<body>
<nav class="navbar-zone">
    <div class="navbar-left">
        <a href="/welcome"><span class="navbar-text-left">
            <img src="${pageContext.request.contextPath}/resources/pictures/Logo/LogoNameRight.png" class="navbar-logo"
                 alt="MITE Logo"></span> </a>
        <a href="/welcome" class="inactive hov-left"><span class="navbar-text-left-word">Home</span> </a>
        <a href="/userList" class="active hov-left"><span class="navbar-text-left-word">User</span> </a>
        <a href="/dataList" class="inactive hov-left"><span class="navbar-text-left-word">Ip</span> </a>
    </div>
    <div class="navbar-right">
        <div class="navbar-right-user">
            <a href="#" class="hov-right">
                <i class="fa fa-user user-icon" aria-hidden="true"></i>
                <span class="navbar-text-right-word">${username}</span>
            </a>
        </div>
        <button class="logout-button" name="logout" onclick="goToLinkLogout()">Logout</button>
    </div>
</nav>
<hr class="line">

<%--CONTENT HERE--%>
<div class="my-content-item">

    <div class="col add-item-content">
        <div class="row add-item-content-header">
            <span class="add-item-title">User: ${first_name}</span>
        </div>
        <div class="row add-item-complete-img">
            <div class="col add-item-complete-zone">
                <div class="col">
                    <div class="row">
                        <span class="input-text">First Name: </span>
                        <span class="input-text-right">${first_name}</span>
                    </div>
                    <div class="row">
                        <span class="input-text">Last Name: </span>
                        <span class="input-text-right">${last_name}</span>
                    </div>
                    <div class="row">
                        <span class="input-text">Email: </span>
                        <span class="input-text-right">${email}</span>
                    </div>
                    <div class="row">
                        <span class="input-text">MAC Address: </span>
                        <span class="input-text-right">${mac_address}</span>
                    </div>
                </div>
                <div class="row add-item-buttons-zone">
                    <button id="editUser" class="add-item-button-edit" name="editUser"><i
                            class="fa fa-edit button-icon"></i><span>Edit</span></button>
                    <button id="deleteUser" class="add-item-button-delete" name="deleteUser"><i
                            class="fa fa-trash button-icon"></i><span>Delete</span></button>
                </div>
            </div>
            <div class="add-item-img-zone">
                <img src="${pageContext.request.contextPath}/resources/pictures/8.png" class="add-item-img"
                     alt="MiteCompleteImg">
            </div>
        </div>
    </div>
</div>

<%--END CONTENT HERE--%>

<div class="bottom-img-zone">
    <img src="${pageContext.request.contextPath}/resources/pictures/7.2.png" class="bottom-img"
         alt="MiteBottomImg">
</div>
</body>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

<script src="${pageContext.request.contextPath}/resources/js/deleteUser.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/updateUser.js"></script>
<script>
    function goToLinkLogout() {
        window.location.href = "/logout"
    }
</script>

</html>
