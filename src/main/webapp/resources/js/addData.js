$(function() {

    $('button[id=addData][type=submit]').click(function(e) {

        e.preventDefault();

        var data = {
            "ip": $('#ip').val(),
            "malicious":$('#malicious').val()
        };

        if (data.malicious === "Unknown"){
            data.malicious = 0;
        }else if (data.malicious === "Malicious"){
            data.malicious = -1;
        }else
        {
            data.malicious = 1;
        }

        $.post({
            url : 'addData',
            data : JSON.stringify(data),
            contentType: "application/json",
            success : function(res) {
                // res = JSON.parse(res);
                if(res.success === "true"){
                    //Set response
                    window.location.href = "/dataList";
                    alert("ADD SUCCES!!")
                }else{
                    //Set error messages
                    let errors = "Error ";
                    let errorsArray = JSON.parse(res.errors);
                    for(let i = 0; i < errorsArray.length; i++) {
                        errors = errors + errorsArray[i] + "\n";
                    }
                    alert(errors);
                }
            }
        })
    });
});
