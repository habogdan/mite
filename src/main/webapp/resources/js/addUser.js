$(function() {

    $('button[id=addUser][type=submit]').click(function(e) {

        //Prevent default submission of form
        e.preventDefault();

        var data = {
            "first_name": $('#first_name').val(),
            "last_name":$('#last_name').val(),
            "email":$('#email').val(),
            "mac_address":$('#mac_address').val()
        };

        $.post({
            url : 'addUser',
            data : JSON.stringify(data),
            contentType: "application/json",
            success : function(res) {
                // res = JSON.parse(res);
                if(res.success === "true"){
                    //Set response
                    window.location.href = "/userList";
                    alert("ADD SUCCES!!")
                }else{
                    //Set error messages
                    let errors = "Error ";
                    let errorsArray = JSON.parse(res.errors);
                    for(let i = 0; i < errorsArray.length; i++) {
                        errors = errors + errorsArray[i] + "\n";
                    }
                    alert(errors);
                }
            }
        })
    });
});
