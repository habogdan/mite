$(function () {

    $('button[id=deleteData]').click(function (e) {

        const queryString = window.location.href;
        let parts = queryString.split("/");
        let id = parts[parts.length - 1];

        $.ajax({
            url: '/data/' + id,
            type: 'DELETE',
            success: function (res) {
                window.location.href = "/dataList";
            },
            error: function (err){
            }
        })
    });
});
