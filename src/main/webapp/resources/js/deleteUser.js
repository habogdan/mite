$(function () {

    $('button[id=deleteUser]').click(function (e) {

        const queryString = window.location.href;
        let parts = queryString.split("/");
        let id = parts[parts.length - 1];

        $.ajax({
            url: '/user/' + id,
            type: 'DELETE',
            success: function (res) {
                window.location.href = "/userList";
            },
            error: function (err){
            }
        })
    });
});
