$(function () {

    $('a[id="login-switch"]').click(function () {
        $('#register-zone').css("display","block");
        $('#login-zone').css("display","none");
    });

    $('a[id="register-switch"]').click(function () {
        $('#register-zone').css("display","none");
        $('#login-zone').css("display","block");
    })

    /*  Submit form using Ajax */
    $('button[id=login][type=submit]').click(function (e) {

        //Prevent default submission of form
        e.preventDefault();

        var data = {
            "username": $('#username_login').val(),
            "password": $('#password_login').val()
        };

        $.post({
            url: 'loginProcess',
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (res) {
                // res = JSON.parse(res);

                if (res.success === "true") {
                    //Set response
                    window.location.href = "/welcome";
                    alert("Welcome!!!");

                } else {
                    //Set error messages
                    alert("Username or password is incorrect");

                }
            }
        })
    });

    //Register Ajax
    $('button[id=register][type=submit]').click(function (e) {

        //Prevent default submission of form
        e.preventDefault();

        var data = {
            "username": $('#username').val(),
            "password": $('#password').val(),
            "email": $('#email').val(),
            "phone_number": $('#phone_number').val()
        };
        console.log(data)
        $.post({
            url: 'registerProcess',
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (res) {
                // res = JSON.parse(res);

                if (res.success === "true") {
                    //Set response
                    window.location.href = "/welcome";
                    alert("Registration successful, you may now login")

                } else {
                    //Set error messages
                    let errors = "Error ";
                    let errorsArray = JSON.parse(res.errors);
                    for (let i = 0; i < errorsArray.length; i++) {
                        errors = errors + errorsArray[i] + "\n";
                    }
                    alert(errors);

                }
            }
        })
    });
});
