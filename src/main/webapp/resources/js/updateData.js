$(function() {

    $('button[id=editData]').click(function (e){
        const queryString = window.location.href;
        let parts = queryString.split("/");
        let id = parts[parts.length - 1];
        window.location.href = "/updateData/"+id
    })

    $('button[id=updateData][type=submit]').click(function(e) {

        const queryString = window.location.href;
        let parts = queryString.split("/");
        let id = parts[parts.length - 1];

        var data = {
            "id_data": id,
            "ip": $('#ip').val(),
            "malicious":$('#malicious').val(),
        };

        if (data.malicious === "Unknown"){
            data.malicious = 0;
        }else if (data.malicious === "Malicious"){
            data.malicious = -1;
        }else
        {
            data.malicious = 1;
        }

        $.ajax({
            url : '/updateData/' + id,
            type: 'PUT',
            data : JSON.stringify(data),
            contentType: "application/json",
            success: function (res) {
                if(res.success === "true") {
                    //Set response
                    window.location.href = "/dataList";
                    alert("UPDATE SUCCES!!");
                }
            },
            error: function (err){
                // console.log(err);
            }
        })
    });
});
