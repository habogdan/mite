$(function() {

    $('button[id=editUser]').click(function (e){
        const queryString = window.location.href;
        let parts = queryString.split("/");
        let id = parts[parts.length - 1];
        window.location.href = "/updateUser/"+id
    })

    $('button[id=updateUser][type=submit]').click(function(e) {

        //Prevent default submission of form
        // e.preventDefault();

        const queryString = window.location.href;
        let parts = queryString.split("/");
        let id = parts[parts.length - 1];

        var data = {
            "id_user": id,
            "first_name": $('#first_name').val(),
            "last_name":$('#last_name').val(),
            "email":$('#email').val(),
            "mac_address":$('#mac_address').val()
        };

        $.ajax({
            url : '/updateUser/' + id,
            type: 'PUT',
            data : JSON.stringify(data),
            contentType: "application/json",
            success: function (res) {
                if(res.success === "true") {
                    //Set response
                    window.location.href = "/userList";
                    alert("UPDATE SUCCES!!");
                }
            },
            error: function (err){
                // console.log(err);
            }
        })
    });
});
