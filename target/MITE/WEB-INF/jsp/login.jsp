<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="${pageContext.request.contextPath}/resources/css/login.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/header.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
</head>
<body>
<!-- HEADER ZONE -->
<nav class="navbar navbar-dark login-header-zone">
    <img src="${pageContext.request.contextPath}/resources/pictures/Logo/LogoNameRight.png" class="login-header-zone-logo"
         alt="MITE Logo">
</nav>
<!-- LOGIN ZONE -->
<div class="container content-zone">
    <div class="row justify-content-center align-items-center">
        <!-- LEFT IMG -->
        <div class="col">
            <img src="${pageContext.request.contextPath}/resources/pictures/4.2.png"
                 class="login-content-logo"
                 alt="MITE Logo">
        </div>
        <!-- LOG/REG ZONE -->
        <div id="login-zone-switch" class="col login-register-zone">
            <div id="register-zone" class="col justify-content-center register-zone">
                <form:form id="regForm" modelAttribute="administrator" action="registerProcess" method="post">

                    <div class="row justify-content-between align-items-end login-max-width">
                        <div class="login-title-create">Create an account</div>
                        <div class="row login-switch-zone">
                            <div class="login-or">or</div>
                            <a href="#"id="register-switch" class="login-or-next" >login in</a>
                        </div>
                    </div>

                    <form:label cssClass="row login-text" path="username">Username</form:label>
                    <form:input cssClass="row login-input" path="username" name="username" id="username"
                                placeholder="Username"/>

                    <form:label cssClass="row login-text" path="password">Password</form:label>
                    <form:input cssClass="row login-input" path="password" name="password" id="password"
                                placeholder="Password" type="password"/>

                    <form:label cssClass="row login-text" path="email">Email</form:label>
                    <form:input cssClass="row login-input" path="email" name="email" id="email"
                                placeholder="Email"/>

                    <form:label cssClass="row login-text" path="phone_number">phone_number</form:label>
                    <form:input cssClass="row login-input" path="phone_number" name="phone_number" id="phone_number"
                                placeholder="Phone_number"/>
                </form:form>
                <div class="row justify-content-center login-button-zone">
                    <button id="register" class="login-register-button" name="register" type="submit">Register</button>
                </div>

            </div>

            <div id="login-zone" class="col justify-content-center login-zone">
                <form:form id="loginForm" modelAttribute="login" action="loginProcess" method="post">

                    <div class="row justify-content-between align-items-end login-max-width">
                        <div class="login-title-create">Sign in</div>
                        <div class="row login-switch-zone">
                            <div class="login-or">or</div>
                            <a href="#" id="login-switch" class="login-or-next">create an account</a>
                        </div>
                    </div>

                    <form:label cssClass="row login-text" path="username">Username</form:label>
                    <form:input cssClass="row login-input" path="username" name="username" id="username_login"
                                placeholder="Username"/>

                    <form:label cssClass="row login-text" path="password">Password</form:label>
                    <form:input cssClass="row login-input" path="password" name="password" id="password_login"
                                placeholder="Password" type="password"/>

                </form:form>
                <div class="row justify-content-center login-button-zone">
                    <button id="login" class="login-register-button" name="login" type="submit">Login</button>
                </div>

            </div>
        </div>
    </div>
</div>

</body>
<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

<script src="${pageContext.request.contextPath}/resources/js/login.js"></script>
</html>
